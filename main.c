#include <msp430fr5994.h>
#include <stdint.h>
#include "radio_drv.h"
#include "cc1x_utils.h"
#include "cc1101_def.h"
#include "hal_spi_rf.h"
#include "hal_timer.h"

// Global Variables

#define TX_BUF_SIZE 1
#define FREQUENCY	902750
unsigned char txBuffer[TX_BUF_SIZE];
extern unsigned char rand_data = 'a';
uint16_t ii;
// Global Functions

void Init_CLK(void);
void Init_Radio(void);
void Init_GPIO(void);


int main(void) {
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	Init_GPIO();

	Init_CLK();
	Init_Radio();

	while (1)
	{
		P5IFG &= ~BIT6; // Clear button flag before going to sleep
		__bis_SR_register(LPM0_bits + GIE);
		__disable_interrupt();
		while (performTX == true)
		{
			//Blinking LED to signal BSL start
			P3OUT ^= (BIT0);
			__delay_cycles(2500000);
			P3OUT ^= (BIT0);
			__delay_cycles(2500000);
			P3OUT ^= (BIT0);
			__delay_cycles(2500000);
			P3OUT ^= (BIT0);
			hal_timer_init(8000);


			txBuffer[0] = 0x01;
			//for (ii = 0; ii < TX_BUF_SIZE - 1; ii++) txBuffer[ii + 1] = 0;

			radio_send(txBuffer, TX_BUF_SIZE);
			hal_timer_stop();

			performTX = false;
			P3OUT ^= (BIT1);
			__delay_cycles(2500000);
			P3OUT ^= (BIT1);
			__delay_cycles(2500000);
			P3OUT ^= (BIT1);
			__delay_cycles(2500000);
			P3OUT ^= (BIT1);
		}
	}
}

//--------------------------- Init CLK -----------------------------//

void Init_CLK(void) {

	CSCTL0_H = CSKEY_H;
	CSCTL1 = DCOFSEL_6;                                     // Set DCO = 8Mhz
	CSCTL2 = SELA__VLOCLK + SELM__DCOCLK + SELS__DCOCLK;
	CSCTL3 = DIVA__1 + DIVS__1 + DIVM__1;

}

//--------------------------- Init Radio -----------------------------//

void Init_Radio(void){

	radio_init(3);

	radio_set_freq(FREQUENCY);

	set_rf_packet_length(TX_BUF_SIZE);

}

//--------------------------- Init GPIO -----------------------------//

void Init_GPIO(void) {
	P5OUT |= (BIT6);
	P5REN |= BIT6;
	P5IES &= ~BIT6;
	P5IFG &= ~BIT6;
	P5IE |= BIT6;

	P1OUT &= ~(BIT0 + BIT1);
	P1DIR |= BIT0 + BIT1;

	P3DIR |= BIT0 + BIT1 + BIT3;
	P3OUT &= ~(BIT0 + BIT1 + BIT3);
	PM5CTL0 &= ~LOCKLPM5;
}

//--------------------------- Port 5 Interrupt Function -----------------------------//
/*#pragma vector=RF_PORT_VECTOR
__interrupt void radio_isr(void) {

	if(RF_GDO_PxIFG & RF_GDO_PIN) {

		// Clear LPM0 bits from 0(SR)
		//__bic_SR_register_on_exit(LPM3_bits);

		// clear the interrupt flag
		RF_GDO_PxIFG &= ~RF_GDO_PIN;

		// indicate that end of packet has been found
		//rf_end_packet = 1;
		P1OUT ^= BIT1                                     ;
	}

	else if(BUTTON_PxIFG & BUTTON1_PIN)
	{
		 // reset the RF_GDO_PIN /
		BUTTON_PxIFG &= ~BUTTON1_PIN;

		// indicate button event /
		P1OUT ^= BIT0;
		radio_send(rand_data, 1);
		// exit from low power mode on ISR exit /
		//__bic_SR_register_on_exit(LPM0_bits);
	}

}*/

