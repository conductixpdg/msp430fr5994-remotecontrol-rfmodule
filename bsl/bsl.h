/* --COPYRIGHT--,BSD
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
#ifndef __BSL_H__
#define __BSL_H__


typedef struct sBSLPayload
{
    uint8_t ui8Command;
    uint8_t ui8Addr_L;
    uint8_t ui8Addr_M;
    uint8_t ui8Addr_H;
    const uint8_t* ui8pData;
} tBSLPayload;

typedef struct sBSLPacket
{
    uint8_t ui8Header;
    uint8_t ui8Length;
    tBSLPayload tPayload;
    uint16_t ui16Checksum;
} tBSLPacket;

/* Prototypes */
// Generic BSL
void BSL_Init(void);
uint8_t BSL_sendCommand(uint8_t cmd);
uint8_t BSL_programMemorySegment(uint32_t addr, const uint8_t* data,
        uint32_t len);
// Communication dependent
void BSL_Comm_Init(void);
uint8_t BSL_getResponse(void);
uint8_t BSL_slavePresent(void);
void BSL_sendSingleByte(uint8_t ui8Byte);
void BSL_sendPacket(tBSLPacket tPacket);
void BSL_flush(void);

#define VBOOT_ENTRY_CMD     0xAA
#define VBOOT_VERSION       0xA1

#define BSL_VERSION_CMD     0x19
#define BSL_ERASE_APP_CMD   0x15
#define BSL_RX_APP_CMD      0x10
#define BSL_JMP_APP_CMD     0x1C

#define BSL_OK_RES          0x00

#define BSL_HEADER          0x80

#endif /* __BSL_H__ */
